//Eye Queue https://gitlab.com/eyequeue/
#include <iostream>
#include "cdk.h"
#include <boost/version.hpp>
#include <boost/thread.hpp>   
#include <boost/date_time.hpp> 
#include <string>
#include <sstream>
#include <fstream>
#include <boost/thread/barrier.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
using namespace std;


#define MATRIX_WIDTH 5
#define MATRIX_HEIGHT 5
#define BOX_WIDTH 19
#define MATRIX_NAME_STRING "Something"
#define NUM_THREADS 25

bool exitnow = false;
boost::barrier barrier(NUM_THREADS+1);
boost::mutex screenLock; // lock for screen
boost::mutex boxbin[NUM_THREADS]; 
boost::thread_group tgroup;
boost::thread *thethread;
boost::mutex errout;
//using namespace std;
//using namespace boost;
int perBinQuant[25];
//boost::interprocess::interprocess_semsphore::perBinSemphore[25];boost::mutex perBinMutex[25];
boost::interprocess::interprocess_semaphore** perBinSem = new boost::interprocess::interprocess_semaphore* [NUM_THREADS]; 





void signal_callback_handler(int sig)
{
  exitnow = true;
  /* for(int i=0;i<26;i++)
    {
      perBinSem[i]->post();
  */ // update_cell(myMatrix,i,k,id,'B',0);
    //thethread = new boost::thread(thread_func, id);

  //    }

 //do stuff to exit....
}
/* Create the matrix.  Need to manually cast (const char**) to (char **)                                                  
void update_cell( int xpos, int ypos, int thread_id, char thread_status, int qty)
{
  sadstringstream otext;
  otext << "tid: " << thread_id << " S: " << thread_status << " Q:" << qty << endl; 
  string cell_text = otext.str();

  
  setCDKMatrixCell(myMatrix, xpos, ypos, cell_text);
  

}
*/
WINDOW *window;
CDKSCREEN *cdkscreen;
CDKMATRIX     *myMatrix;           // CDK Screen Matrix  
;
  
   
  // int i = id/25
// update_cell(myMatrix,myID/5,myID%5,myID,'S',perBinQuant[myID]);
   
  //now we should change the status to waiting on...

  // cout << "Thread " << myID << " now performing work...\n" << flush;/


   



/* Create the matrix.  Need to manually cast (const char**) to (char **)     */
void update_cell(CDKMATRIX *myMatrix,  int xpos, int ypos, int thread_id, char thread_status, int qty)
{
  // errout.lock();
  screenLock.lock();
  stringstream otext;
  otext << "tid: " << thread_id << " S: " << thread_status << " Q:" << qty << endl;
  string cell_text = otext.str();
  // cout << cell_text << endl;                                                                                     
  setCDKMatrixCell(myMatrix, xpos, ypos, cell_text.c_str());
  // setCDKMatrixCell(myMatrix, xpos, ypos, "no");                                                                  
  drawCDKMatrix(myMatrix, true);
  screenLock.unlock();
  // errout.unlock();
}

void thread_func(int myID)
{
  errout.lock();
   cerr << "Thread " << myID << " waiting on barrier...\n" << flush;                                              
   errout.unlock();
   perBinQuant[myID] = 0;
  barrier.wait();
  int xcoord[25] = {1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,4,4,4,4,4,5,5,5,5,5};
  int ycoord[25] = {1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5};
  //int ycoord[myId];

  //int tempid = myID;
  // int i = id/25
  errout.lock();
  cerr << "Thread " << myID << " waiting on barrier...\n" << flush;
  //  update_cell(myMatrix,(tempid/6),(tempid%6)-1,myID,'S',perBinQuant[myID]);
  // update_cell(myMatrix,2,4,myID,'S',perBinQuant[myID]);  
  //now we should change the status to waiting on...                                                                
  errout.unlock();

  while(!exitnow)
    { 
 update_cell(myMatrix,xcoord[myID],ycoord[myID],myID,'W',perBinQuant[myID]); 
 errout.lock(); 
 cerr << "Thread " << myID << " now performing work...\n" << flush;
 errout.unlock();
 //errout.unlock();
 perBinSem[myID]->wait();
 /*
 if(perBinQuant[myID] == 0)
   {  
     update_cell(myMatrix,xcoord[myID],ycoord[myID],myID,'E',perBinQuant[myID]);
 break;
   }
 */

  boxbin[myID].lock();
  if(perBinQuant[myID]!=0)
 perBinQuant[myID]--;
 boxbin[myID].unlock();                                        

  update_cell(myMatrix,xcoord[myID],ycoord[myID],myID,'C',perBinQuant[myID]);
  sleep(1);
    }
  errout.lock();
  cerr << "Thread " << myID << " see if. we can get rid of the items inside....\n" << flush;
  errout.unlock();
  while(perBinQuant[myID] >0)
    {boxbin[myID].lock();
    perBinQuant[myID]--;
    boxbin[myID].unlock();
    update_cell(myMatrix,xcoord[myID],ycoord[myID],myID,'C',perBinQuant[myID]);
    sleep(1);
}
  if(perBinQuant[myID]==0)
    update_cell(myMatrix,xcoord[myID],ycoord[myID],myID,'E',perBinQuant[myID]);
  
 //errout.unlock();
}



int main()
{
  
  /*
  WINDOW *window;
  CDKSCREEN *cdkscreen;
  CDKMATRIX     *myMatrix;           // CDK Screen Matrix
  */

  //capture signals
  signal(SIGINT, signal_callback_handler);
  signal(SIGTERM, signal_callback_handler);
  //  boost::mutex screenLock; // lock for mutex


  for(int i=0; i<NUM_THREADS;i++)
    {
      boost::interprocess::interprocess_semaphore* p = new boost::interprocess::interprocess_semaphore(0);
      // boost::interprocess::interprocess_semaphore** perBinSem[i] = boost::interprocess::interprocess_semaphore p;
      // boost::interprocess::interprocess_semaphore* perBinSem[i] =  p;      
      perBinSem[i] =  p;
 }


 
  // bool exitnow = false;
  void update_cell(CDKMATRIX *myMatrix, int xpos, int ypos, int thread_id, char thread_status, int qty); 
  const char *rowTitles[MATRIX_HEIGHT+1] = {"0","1","2", "3", "4", "5"};
  const char *columnTitles[MATRIX_WIDTH+1] = {"0","1","2", "3", "4", "5"};
  int boxWidths[MATRIX_WIDTH+1] = {BOX_WIDTH, BOX_WIDTH, BOX_WIDTH, BOX_WIDTH, BOX_WIDTH, BOX_WIDTH};
  int boxTypes[MATRIX_WIDTH+1] = {vMIXED, vMIXED, vMIXED, vMIXED, vMIXED, vMIXED};
 
/*
   * Initialize the Cdk screen.
   *
   * Make sure the putty terminal is large enough

   */
  
  window = initscr();
 cdkscreen = initCDKScreen(window);

 /* Start CDK Colors */
  initCDKColor();

 /* Create the matrix.  Need to manually cast (const char**) to (char **)
  */
 
   myMatrix = newCDKMatrix(cdkscreen, CENTER, CENTER, MATRIX_WIDTH, MATRIX_HEIGHT, MATRIX_WIDTH, MATRIX_HEIGHT,
			   MATRIX_NAME_STRING, (char **) columnTitles, (char **) rowTitles, boxWidths,
			   boxTypes, 1, 1, ' ', ROW, true, true, false);
  

 if (myMatrix ==NULL)
   {
     printf("Error creating Matrix\n");
     _exit(1);
     } 
 /* Display the Matrix */
 drawCDKMatrix(myMatrix, true);

/* Display a message
   */
 // setCDKMatrixCell(myMatrix, 2, 2, "Test Message");
 // setCDKMatrixCell(myMatrix, 5, 5, "meh");
   // drawCDKMatrix(myMatrix, true);
    /* required  */
 int id = 0;

 
 // for(int id =0; id < NUM_THREADS;id++)  
    for(int i=1; i < 6; i++)
     {  for(int k=1; k < 6;k++)
     {
       update_cell(myMatrix,i,k,id,'B',0);
       thethread = new boost::thread(thread_func, id);
       tgroup.add_thread(thethread);
       id++;
     }
     }
 
    // update_cell(myMatrix,3,3,3,'B',0);
   drawCDKMatrix(myMatrix, true);
   //screenLock.lock();

 /* so we can see results */
   // sleep (10);
   // update_cell(myMatrix,2,4,4,'B',0);
   // update_cell(myMatrix,i,k,4,'B',0); 
sleep(5);
 int valueinbin = rand() % (21 - 10) + 10;
 int r = rand()% 25;
 // int xcoord[25] = {1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,4,4,4,4,4,5,5,5,5,5};
 // int ycoord[25] = {1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5};
 barrier.wait();  /* release the threads */

 errout.lock();
 cerr<< r<< " passed barrier..." << flush;
 // cerr <<valueinbin<< " value of thing to go in bin..." << flush;
 // update_cell(myMatrix,xcoord[myID],ycoord[myID],myID,'W',perBinQuant[myID])                                                 
 errout.unlock();

 sleep(5);

     srand(time(NULL)); //only call 1


 while(exitnow==false)
   {
     //workto do.....
     errout.lock();
     cerr <<"adding stuff to bins."<<flush;
     errout.unlock();
     r = rand()% 25;
     valueinbin = rand() % (21 - 10) + 10;
     // boxbin[r].lock();

     //for(int i =0; i< valueinbin;i++)
     //{
     errout.lock();
	 cerr<< r<< " which bin..." << flush;
	 cerr <<valueinbin<< " value of thing to go in bin..." << flush;
	 // update_cell(myMatrix,xcoord[myID],ycoord[myID],myID,'W',perBinQuant[myID])	 
	 	 errout.unlock();
	 // perBinSem[r]->post();
	 boxbin[r].lock();
	 // update_cell(myMatrix,xcoord[myID],ycoord[myID],myID,'W',perBinQuant[myID]);  
	 perBinQuant[r] += valueinbin;
	 boxbin[r].unlock();
	 for(int i = 0; i<valueinbin; i++)
	    perBinSem[r]->post();
	 //update_cell(myMatrix,xcoord[r],ycoord[r],r,'C',perBinQuant[r]);
	 //boxbin[r].unlock();
	 //}
     // boxbin[r].unlock();
	 sleep(1);
 }

 for(int i=0;i<25;i++)                                                                                                                
   {                                                                                                                                     
     perBinSem[i]->post(); 
   }
 /* Now wait for all threads to exit */
 tgroup.join_all();
 sleep(1);
 errout.lock();
 cerr << "Parent exiting\n" << flush;
 errout.unlock();

// Cleanup screen
 endCDK();
 //sleep(10);

 // screenLock.unlock();   //unlock it...
cout << "something cool" << endl;
 

}
/*
 Create the matrix.  Need to manually cast (const char**) to (char **)     */
/*
void update_cell(CDKMATRIX *myMatrix,  int xpos, int ypos, int thread_id, char thread_status, int qty)
{
  screenLock.lock();
  stringstream otext;
  otext << "tid: " << thread_id << " S: " << thread_status << " Q:" << qty << endl;
  string cell_text = otext.str();
  // cout << cell_text << endl;
   setCDKMatrixCell(myMatrix, xpos, ypos, cell_text.c_str());
  // setCDKMatrixCell(myMatrix, xpos, ypos, "no");
  drawCDKMatrix(myMatrix, true);
  screenLock.unlock();

}
*/
/*
void update_cell( int xpos, int ypos, int thread_id, char thread_status, int qty)
{
  screenLock.lock();
  stringstream otext;
  otext << "tid: " << thread_id << " S: " << thread_status << " Q:" << qty << endl;
  string cell_text = otext.str();
  // cout << cell_text << endl;                                                                                                   
  setCDKMatrixCell(myMatrix, xpos, ypos, cell_text.c_str());
  // setCDKMatrixCell(myMatrix, xpos, ypos, "no");                                                                                
  drawCDKMatrix(myMatrix, true);
  screenLock.unlock();

}
*/
/*
void thread_func(int myID)
{
  // cout << "Thread " << myID << " waiting on barrier...\n" << flush;                                              
  perBinQuant[myID] = 0;
  barrier.wait();


  // int i = id/25                                                                                                  
  update_cell(myMatrix,myID/5,myID%5,myID,'S',perBinQuant[myID]);

  //now we should change the status to waiting on...                                                                

  // cout << "Thread " << myID << " now performing work...\n" << flush;                                             
}
*/
